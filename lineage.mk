#
# Copyright (C) 2014 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit some common CM stuff
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

$(call inherit-product, device/samsung/flteskt/full_flteskt.mk)

PRODUCT_DEVICE := flteskt
PRODUCT_NAME := lineage_flteskt

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=flteskt \
    PRODUCT_DEVICE=flteskt \
    TARGET_DEVICE=flteskt \
    PRODUCT_MODEL=SM-G910S \
    BUILD_FINGERPRINT=samsung/flteskt/flteskt:4.4.2/KOT49H/G910SKSUENG5:user/release-keys \
    PRIVATE_BUILD_DESC="flteskt-user 4.4.2 KOT49H G910SKSUENG5 release-keys"
